#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_ROTATE_H

#include "image.h"
#include "bmp_file.h"

struct image * rotate(const struct image *  img);

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_ROTATE_H
