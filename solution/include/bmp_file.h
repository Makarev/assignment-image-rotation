#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>

#include "image.h"
#include "status.h"

struct bmp_header;

struct bmp_header * create_header();

void destroy_header(struct bmp_header * header);

struct bmp_header * generate_header_to_image(const struct bmp_header * prev_header, const struct image * img);

enum read_status read_bmp_header(FILE * in, struct bmp_header * header);

enum write_status write_bmp_header(FILE * out, const struct bmp_header * header);

uint64_t get_width_bmp(const struct bmp_header * header);

uint64_t get_height_bmp(const struct bmp_header * header);


#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_BMP_H
