#ifndef ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>


#include "status.h"

struct image;

struct pixel;

struct pixel * create_pixels(uint64_t width, uint64_t height);

void destroy_pixels(struct pixel * pixels);

struct image * create_image();

void destroy_image(struct image * img);

uint64_t get_width(const struct image * img);

uint64_t get_height(const struct image * img);

struct pixel * get_pixel(const struct image * img);

struct pixel * get_current_pixel(const struct image * img, size_t index);

void set_current_pixel(struct image * img, size_t index, struct pixel * value);

void set_width(struct image * img, uint64_t width_);

void set_height(struct image * img, uint64_t height_);

void set_pixel(struct image * img, uint64_t data_size);

long get_padding(const struct image * img);

uint64_t get_data_pixel_size(uint64_t width, uint64_t height);

size_t get_pixel_size();

enum read_status read_image(FILE * in , struct image * img);

enum write_status write_image(FILE * out, struct image * img);

#endif //ASSIGNMENT_IMAGE_ROTATION_MASTER_IMAGE_H
