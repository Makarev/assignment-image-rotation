#include <stdio.h>

#include "rotate.h"

struct image * rotate(const struct image *  img){
    struct image * new_img = create_image();
    const struct image * prev_image = img;
    set_height(new_img, get_width(prev_image));
    set_width(new_img, get_height(prev_image));
    set_pixel(new_img, get_data_pixel_size(get_width(prev_image), get_height(prev_image)));
    for (uint64_t i = 0; i < get_height(prev_image); i++) {
        for (uint64_t j = 0; j < get_width(prev_image); j++) {
            set_current_pixel(new_img, (get_height(img) - 1 - i) + get_height(img) * j, get_current_pixel(img, get_width(img) * i + j));
        }
    }
    return new_img;
}
