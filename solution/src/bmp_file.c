#include "bmp_file.h"

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

struct bmp_header * create_header() {
    return (struct bmp_header *) malloc(sizeof(struct bmp_header));
}

void destroy_header(struct bmp_header * header){
    free(header);
    header = NULL;
}

uint64_t get_width_bmp(const struct bmp_header * header) {
    return header->biWidth;
}

uint64_t get_height_bmp(const struct bmp_header * header) {
    return header->biHeight;
}

enum read_status read_bmp_header(FILE * in, struct bmp_header * header) {
    if (in != NULL) {
        if (header != NULL) {
            fread(header, sizeof(struct bmp_header), 1, in);
            return READ_OK;
        }
        return READ_INVALID_BITS;
    }
    return READ_INVALID_HEADER;
}

enum write_status write_bmp_header(FILE * out, const struct bmp_header * header){
    if (out != NULL) {
        if (header != NULL) {
            fwrite(header, sizeof(struct bmp_header), 1, out);
            return WRITE_OK;
        }
    }
    return WRITE_ERROR;
}

struct bmp_header * generate_header_to_image(const struct  bmp_header * prev_header, const struct image * img) {
    struct bmp_header *new_header = create_header();
    new_header->bfType = prev_header->bfType;
    new_header->bfileSize =
            get_width(img) * get_height(img) * get_pixel_size() + get_height(img) * get_padding(img) +
            sizeof(struct bmp_header);
    new_header->bfReserved = prev_header->bfReserved;
    new_header->bOffBits = prev_header->bOffBits;
    new_header->biSize = prev_header->biSize;
    new_header->biWidth = prev_header->biHeight;
    new_header->biHeight =  prev_header->biWidth;
    new_header->biPlanes = prev_header->biPlanes;
    new_header->biBitCount = prev_header->biBitCount;
    new_header->biCompression = prev_header->biCompression;
    new_header->biSizeImage = new_header->bfileSize - new_header->bOffBits;
    new_header->biXPelsPerMeter = prev_header->biXPelsPerMeter;
    new_header->biYPelsPerMeter = prev_header->biYPelsPerMeter;
    new_header->biClrUsed = prev_header->biClrUsed;
    new_header->biClrImportant = prev_header->biClrImportant;
    return new_header;
}

