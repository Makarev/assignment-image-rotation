#include "util.h"


FILE * open_file(const char * filename, const char * options) {
    FILE * file;
    file = fopen(filename, options);
    return file;
}

void close_file(FILE * file){
    fclose(file);
}
