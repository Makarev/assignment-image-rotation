#include <stdlib.h>

#include "drawing_bmp.h"

struct drawing_image  {
   struct bmp_header * header_;
   struct image  * image_;
};

struct drawing_image *create_drawing_image() {
    return (struct drawing_image *) malloc(sizeof(struct drawing_image));
}

void destroy_drawing_image(struct drawing_image *draw) {
    free(draw);
    draw = NULL;
}

struct bmp_header * get_bmp_header(struct drawing_image *draw) {
    return draw->header_;
}

struct image *get_image(struct drawing_image *draw) {
    return draw->image_;
}

void set_bmp_header(struct drawing_image *draw, struct bmp_header *header) {
    draw->header_ = header;
}

void set_image(struct drawing_image *draw, struct image *image) {
    draw->image_ = image;
}


